<?php

namespace App\Controllers;

use Config\Services;


class Home extends BaseController
{
    protected $auth;
    protected $session;
    
    public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger)
    {
        parent::initController($request, $response, $logger);
        //------------------------------------------------------------
        // Preload any models, libraries, etc, here.
        //------------------------------------------------------------
        $this->session = Services::session();
        $this->auth = new \IonAuth\Libraries\IonAuth();

    }


    public function index() {
        $familias = new \App\Models\FamiliaModel();;

        $data['familias'] = $familias->findAll();

        echo view ('Articulos/Catalogo',$data);
    print_r($data);
    }
    
 
  public function catalogo() {
        $familias = new \App\Models\FamiliaModel();
        $titulo['titulo'] = "CATALOGO";
        $data['familias'] = $familias->SELECT("familias.NombreFamilia ,familias.CodigoFamilia as Cod_fam, productos.CodigoProducto as cod , productos.Nombre , productos.CodigoFamilia , productos.CodigoLinea")
                ->join('productos', 'familias.CodigoFamilia = productos.CodigoFamilia', 'LEFT')
                ->groupby('CodigoFamilia')
                ->findAll();
        
        echo view ('Articulos/Catalogo',$data);
    }

 
    
}
